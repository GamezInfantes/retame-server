var _ = require('underscore')


function generateREST(router, name, Model, roles, pupulateFields=""){
  function addRoleMiddleware(req, res, next){
    let role = _.findWhere(roles, {name: req.user.role });
    if(role.allow.create=='*')
    role.allow.create = Object.keys(Model.schema.paths);
    if(role.allow.read=='*')
    role.allow.read = Object.keys(Model.schema.paths);
    if(role.allow.update=='*')
    role.allow.update = Object.keys(Model.schema.paths);

    req.role = role;
    next();
  }
  function addSelect(query, role){
    query.select(role.allow.read.join(' '))
  }

    var REST = {
      getAll: function (req, res) {
        let query = Model.find().populate(pupulateFields);
        addSelect(query, req.role);

        query.exec().then((models)=>{
          res.json({data: models, success: true});
        })
        .catch((err)=>{
          res.json({message: err, success: false});
        });
      },


      getOne: function (req, res) {
        let query = Model.findOne({_id:req.params.id})
        addSelect(query, req.role);

        query.exec().then((model)=>{
          res.json({data: model, success: true});
        })
        .catch((err)=>{
          res.json({message: err, success: false});
        });
      },
      create: function (req, res) {
        new Promise(function(resolve, reject) {
          // check body in allow.create
          if (_.intersection(_.keys(req.body), req.role.allow.create).length == Object.keys(req.body).length){
            resolve( Model.create(req.body) );
          } else {
            reject(new Error('Permission denied'));
          }
        })
        .then((model)=>{
          res.json({data: model});
        })
        .catch((err)=>{
          res.json( {status: 'fail', error:err})
        });
      },
      delete: function(req, res){
        new Promise(function(resolve, reject) {
          if (req.role.allow.delete){
            resolve( Model.remove({ _id: req.params.id }).exec() );
          } else {
            reject(new Error('Permission denied'));
          }
        })
        .then(()=> {
          res.json({ success: true });
        })
        .catch((err)=>{
          res.json( {status: 'fail', message:err})
        });
      },
      update: function (req, res) {
        new Promise(function(resolve, reject) {
          // check body in allow.update
          if (_.intersection(_.keys(req.body), req.role.allow.update).length == Object.keys(req.body).length) {
            resolve( Model.findOneAndUpdate({_id: req.params.id}, req.body).exec() )
          } else {
            reject('Permission invalid atrivutes');
          }
        })
        .then((model)=> {
          res.json({ success: true, data: model});
        })
        .catch((err)=>{
          res.json( {status: 'fail', message:err})
        });
      }
    };
    // router.route(`/${name}/*`).all(addRoleMiddleware)
    router.route(`/${name}/`)
      .get(addRoleMiddleware, REST.getAll)
      .post(addRoleMiddleware, REST.create)

    router.route(`/${name}/:id/`)
      .get(addRoleMiddleware, REST.getOne)
      .delete(addRoleMiddleware, REST.delete)
      .put(addRoleMiddleware, REST.update);
  }

module.exports =  { generateREST }
