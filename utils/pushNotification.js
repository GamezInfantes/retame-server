'use strict'

// import config from 'server/config'
var config = require('server/config');
var gcm = require('node-gcm');

var sender = new gcm.Sender(config.GOOGLE_SERVER_API_KEY);


class PushNotification {
  constructor(message) {
    var defaults = {
      priority: 'high',
      contentAvailable: true,
    }
    this.message = new gcm.Message(Object.assign(defaults, message));
  }


  send(registrationTokens){
    return new Promise( (resolve, reject) => {
      console.log(config.GOOGLE_SERVER_API_KEY);
      var sender = new gcm.Sender(config.GOOGLE_SERVER_API_KEY);
      sender.sendNoRetry(this.message, { registrationTokens: registrationTokens }, function(err, response) {
        if(err){
          reject(err);
        }
        else { resolve(response); }
      });
    });
  }
}

// export default PushNotification
module.exports = PushNotification;
