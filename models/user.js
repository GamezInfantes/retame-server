'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var validate = require('mongoose-validator');
var bcrypt = require('bcrypt-nodejs');
var _ = require('underscore');
var Dare = require('./dare');


var userSchema = new Schema ({
    email: { type: String, unique: true, required: true, validate: [
      validate({
        validator: 'isEmail',
        message: "It isn't an email adress"
      }),
      ]
    },
    username: { type: String, unique: true, required: true, validate: [
      validate({
        validator: 'isLength',
        arguments: [3, 50],
        message: '{VALUE} should be between {ARGS[0]} and {ARGS[1]} characters'
      }),
    ] },
    password: { type: String },
    role: { type: String, default: 'player' },
    displayName: {type: String, validate: [
      validate({
        validator: 'isLength',
        arguments: [3, 50],
        message: '{VALUE} should be between {ARGS[0]} and {ARGS[1]} characters'
      })
    ]},
    image: { type: Number, default: 0 },
    gender: { type: String, enum: ['male', 'female' ], default:'male' },
    couple: { type: Schema.Types.ObjectId, ref: 'User'},
    points: { type: Number, default: 0 },
    favoriteDares: [ {type: Schema.Types.ObjectId, ref: 'Dare', default:[] } ],
    completedBefore: [ {type: Schema.Types.ObjectId, ref: 'Dare', default:[] } ],
    dares: [{
      dare: { type: Schema.Types.ObjectId, ref: 'Dare'},
      sendedAt: { type: Date },
      acceptedAt: { type: Date },
      duration: { type: String, enum: ['now', 'day', 'week', '2weeks']}
    }],
    follow: [ { type: Schema.Types.ObjectId, ref: 'Dare' } ],
    followers: { type: Number, default: 0},
    settings: {
      notifications: {
        app: { type: Boolean, default: true },
        couple: { type: Boolean, default: true }
      }
    },
    categoriesUnlooked: [{ type: Schema.Types.ObjectId, ref: 'Category' }],
    devices: [{
      registrationId: { type: String }
    }],
  });


userSchema.set('toJSON', {
  virtuals: true,
  transform: function (doc, ret, options) {
     delete ret._id;
     delete ret.__v;
  }
});

// Execute before each user.save() call
userSchema.pre('save', function(callback) {
  var user = this;

  // Break out if the password hasn't changed
  if (!user.isModified('password')) return callback();

  // Password changed so we need to hash it
  bcrypt.genSalt(5, function(err, salt) {
    if (err) return callback(err);

    bcrypt.hash(user.password, salt, null, function(err, hash) {
      if (err) return callback(err);
      user.password = hash;
      callback();
    });
  });
});

userSchema.methods.verifyPassword = function(password, callback) {
  bcrypt.compare(password, this.password, function(err, isMatch) {
    if (err) return callback(err);
    callback(null, isMatch);
  });
};

/**
 * Return info can be expose
 */
userSchema.methods.public = function() {
  var userKeys = [
    'id', 'email', 'username', 'displayName', 'image', 'gender', 'points',
    'favoriteDares', 'completedBefore', 'dares', 'follow', 'followers', 'settings', 'role'
  ]
  var coupleKeys = [
    'id', 'email', 'username', 'displayName', 'image', 'gender', 'points', 'dares'
  ]

  var publicUser = _.pick(this, userKeys);
  publicUser.couple = _.pick(this.couple, coupleKeys);
  return publicUser;
};


module.exports = mongoose.model('User', userSchema);
