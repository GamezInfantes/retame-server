var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var validate = require('mongoose-validator');
var _ = require('underscore');
var Dare = require('./dare');

var categorySchema = new Schema ({
  name: {type: String, validate:[
    validate({
      validator: 'isLength',
      arguments: [1, 140],
      message: '{VALUE} should be between {ARGS[0]} and {ARGS[1]} characters'
    })
  ]},
  image: String, // url
  // dares: [{ type : Schema.Types.ObjectId, ref: 'Dare' }],
  _dares: [{
    target: { type: String, required: true, enum: ['male-female', 'female-male'] },
    dares: [{ type : Schema.Types.ObjectId, ref: 'Dare' }],
  }],
  published: { type: Boolean, default: false },
  looked: { type: Boolean, default: false },
});

categorySchema.set('toJSON', {
  virtuals: true,
  transform: function (doc, ret, options) {
     delete ret._id;
     delete ret.__v;
  }
});


categorySchema.methods.targered = function(user) {
  var target = this.constructor.getTarget(user);
  var _dares = this._dares.find((element, index, array)=>{
    console.log(element.target == target);
    return element.target == target;
  });
  if (_dares) {
    this.virtualDares = _dares.dares
  }
}
// TODO: virtual dares with targered dares id
categorySchema.virtual('dares').get(function(){
  return this.virtualDares || [];

  try {
    return this._dares[0].dares;
  } catch (e) {
    return [];
  }
});
categorySchema.virtual('dares_count').get(function(){
  try {
    return this.virtualDares.length;
  } catch (e) {
    return 0;
  }
});

categorySchema.statics.targered = function(user) {
  // TODO: getTarget
  if(user.gender == "male"){
    var target = 'male-female';
  } else if (user.gender == "female"){
    var target = 'female-male';
  }
  return this.find({
    published: true,
  });
}

categorySchema.statics.getTarget = function(user) {
  // TODO: getTarget
  if(user.gender == "male"){
    var target = 'male-female';
  } else if (user.gender == "female"){
    var target = 'female-male';
  }
  return target;
}

module.exports = mongoose.model('Category', categorySchema);
