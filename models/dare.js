'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var validate = require('mongoose-validator');
var _ = require('underscore');
var PushNotification = require('server/utils/pushNotification');
var Promise = require('bluebird');



var dareSchema = new Schema ({
      title: {type: String,  required: true, validate:[
        validate({
          validator: 'isLength',
          arguments: [3, 140],
          message: '{VALUE} should be between {ARGS[0]} and {ARGS[1]} characters'
        })
      ]},
      description: {type: String, required: true, validate:[
        validate({
          validator: 'isLength',
          arguments: [1, 3000],
          message: '{VALUE} should be between {ARGS[0]} and {ARGS[1]} characters'
        })
      ]},
      points: { type: Number, required: true },
      timesCompleted: { type: Number, default:0 },
      timesSended: { type: Number, default:0 },
      timesFavorite: { type: Number, default:0 },
      author: {
        id: { type: Schema.Types.ObjectId, ref: 'User'},
        username: { type: String },
        image: { type: Number }
      },
      image: { type: String },
      published: { type: Boolean, default: false },
      language: { type: String, default: 'es' },
      target: { type: String },
      type: { type: String, enum: ['comunity', 'category', 'writted' ]}
    });

dareSchema.set('toJSON', {
  virtuals: true,
  transform: function (doc, ret, options) {
     delete ret._id;
     delete ret.__v;

  }
});


dareSchema.statics.findPublic = function (options) {
  return this.find(_.extends({published: true}, options)).exec().then(function(dares){

    var publicDares = _.map(dares, function(dare){
      return {
        id: dare.id,
        title: dare.title,
        description: dare.description,
        points: dare.points,
        views: dare.views,
        timesSended: dare.timesSended,
        author: {
          username: dare.username,
          image: dare.image
        }
      };
    });
    return publicDares;

  });
}

dareSchema.methods.send = function (user, duration) {
  // Get Dare
  // TODO: Check if can see dare
  //
  user.couple.dares.push({
    dare: this.id,
    sendedAt: Date.now(),
    duration: duration
  });
  this.timesSended++;
  this.save();

  return user.couple.save();

  // TODO: Notify couple devices
}
/**
 * Add or remove favaore to provided user
 * @method toogleFavorite
 * @param  {User} user User instance
 * @return {[type]} [description]
 */
dareSchema.methods.toogleFavorite = function(user) {
  let index = user.favoriteDares.indexOf(this._id);
  let isFavorite = index > -1;
  if(isFavorite) {
    user.favoriteDares.splice(index, 1);
    this.timesFavorite = this.timesFavorite>0 ? --this.timesFavorite : 0;
  } else {
    user.favoriteDares.push(this._id);
    this.timesFavorite++;
  }
  this.save();
  return user.save();
}


dareSchema.statics.accept = function(user, indexDares) {
  if (typeof user.dares[indexDares].acceptedAt === "undefined") {
    user.dares[indexDares].acceptedAt = Date.now();
    return user.save().then((user)=>{
      //notify accepted
      var notification = new PushNotification({
        data: {
          message: `${user.displayName} a aceptado tu reto`,
          acction: 'dare_accept',
          index: indexDares,
          acceptedAt: user.dares[indexDares].acceptedAt,
        },
      });
      notification.send([user.couple.devices[0].registrationId]).catch((err)=>{console.log(err);});
      return user;
    })
  }
  return Promise.reject({message: "Dares was acceted"})

}

dareSchema.statics.reject = function(user, indexDares) {
  var notification = new PushNotification({
    data: {
      message: `${user.displayName} a rechazado tu reto`,
      acction: 'dare_reject',
      index: indexDares,
    },
  });
  // remove dare from list
  user.dares.splice(indexDares, 1);
  return user.save().then((user)=>{
    //notify reject
    notification.send([user.couple.devices[0].registrationId]).catch((err)=>{});
    return user;
  });
}


dareSchema.statics.complete = function (user, indexDares){
  var notification = new PushNotification({
    data: {
      message: `¡Enhorabuena! has completado el reto`,
      acction: 'dare_completed',
      index: indexDares,
    },
  });

  if(user.couple.dares[indexDares]){
    var dareId = user.couple.dares[indexDares].dare;
  } else {
    return Promise.reject({message: "Invalid index"});
  }

  //getDare from drareId
  return this.findOne({_id: dareId}).exec()
  //increase counter
  .then((dare)=>{
    dare.timesCompleted++
    return dare.save()
  })
  // update couple values
  .then(function(dare){
    user.couple.dares.splice(indexDares, 1);
    user.couple.points = user.couple.points + dare.points;
    return user.couple.save();
  })
  .then((couple)=>{
    // notify couple
    notification.send([user.couple.devices[0].registrationId]).catch((err)=>{});
    return couple;
  });
}

dareSchema.statics.fail = function (user, indexDares){
  var notification = new PushNotification({
    data: {
      message: `¡Vaya! no has cumplido el reto`,
      acction: 'dare_fail',
      index: indexDares,
    },
  });

  if(user.couple.dares[indexDares]){
    var dareId = user.couple.dares[indexDares].dare;
  } else {
    return Promise.reject({message: "Invalid index"});
  }
  //getDare from drareId
  return this.findOne({_id: dareId}).exec()
  .then((dare)=>{
    user.couple.dares.splice(indexDares, 1);
    user.couple.points = user.couple.points - dare.points;
    if(user.couple.points < 0 ) user.couple.points=0;
    return user.couple.save();
  })
  then((couple)=> {
    notification.send([user.couple.devices[0].registrationId]).catch((err)=>{});
    return couple;
  })
}


module.exports = mongoose.model('Dare', dareSchema);
