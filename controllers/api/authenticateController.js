var jwt = require('jsonwebtoken');
var config = require('server/config');
var User = require('server/models/user');

// create a token
function generateToken(userId) {
  return jwt.sign( {userId: userId} , config.JWT_SECRET, {
    expiresIn: 24*60*60 // expires in 1 day
  });
}
function generateRefreshToken(userId) {
  return jwt.sign( {userId: userId} , config.JWT_SECRET, {});
}

module.exports = {
  index: function(req, res){

  
    console.log(req.body);
setTimeout(function (params) {
    // find the user
    User.findOne( {email: req.body.email }).exec()
    .then((user) => {
      if (!user) {
        res.status(401).json({ status: 'fail', message: 'Authentication failed. Email not found.' });
      } else if (user) {
        // check if password matches
        user.verifyPassword(req.body.password, (err, match) => {
          if (!match) {
            res.json({ status: 'fail', message: 'Authentication failed. Wrong password.' });
          } else {
            res.json({ status: 'success', data: {
              token: generateToken(user.id),
              refresh: generateRefreshToken(user.id),
            }});
          }
        })
      }
    })
    .catch(() => { res.send(err) });
}, 5000);

  },

  refresh: function(req, res){
    const { refresh } = req.body;
    try {
      var decoded = jwt.verify(refresh, config.JWT_SECRET, {
        ignoreExpiration: true,
      });
      res.json({ status: 'success', data:{
        token: generateToken(decoded.userId),
      }})
    } catch (err) {
      res.status(401).json({ status: 'fail'});
    }

  }
};
