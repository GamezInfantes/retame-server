'use strict'

var _ = require('underscore');
var Dare = require('../../models/dare');
var Promise = require('bluebird');


var acctionPattern = function (acction){
   return function (req, res) {
     const { index } = req.body;
     if(typeof index !== "undefined"){
       var prom = Dare[acction](req.user, index);
     } else {
       var prom = Promise.reject({message:"Invalid params"});
     }
     prom.then(()=>{
       res.json({status: "success"});
     })
     .catch((err)=>{
       res.json(err);
       console.log('error')
     });
   }
}


function getAll(req, res) {
  const { author } = req.headers;
  const { user } = req;

  // if (author){
  //   $and: [{ 'author.id': author }],
  // }
  // if(published){
  //   $and: [{ published: published }],    
  // }
  let queryPublished = true; 
  if(user.role != 'player'){
    queryPublished = published || true;
  }

  Dare.find({
    $or: [
      { $and: [{type: 'comunity'}, {published: queryPublished}] },
      { $and: [{type: 'category'}, {published: queryPublished}] },
      { $and: [
        {type: 'writted'},
        {'author.id': { $in: [ user._id, user.couple._id ] }}
      ]}
    ]
  })
  .select(
    `title description points image language
    timesFavorite timesSended timesCompleted published`
  )
  .then((dares)=>{
    res.json({
      status: 'success',
      data: dares
    });
  });
}

function getOne(req, res) {
  Dare.findOne({
    _id: req.params.id
  })
  .select(
    `title description points image language
    timesFavorite timesSended timesCompleted published`
  )
  .exec()
  .then((dare)=>{
    res.json({
      status: 'success',
      data: dare
    });
  });
}



/** Moderators update dares */
function update(req, res) {
  if (req.user.role == 'admin') {
    Dare.findOne({
      _id: req.params.id
    })
    .then((dare)=>{
      var values = _.pick(req.body, 'title', 'description', 'points', 'published');
      Object.assign(dare, values);
      return dare.save();
    }).then((dare)=>{
      res.json({
        status: 'success',
        data: dare
      });
    })
  } else {
    res.status(403).json({
      status: 'fail',
    })
  }
}


function toogleFavorite(req, res){
  Dare.findOne({_id: req.params.id}).exec().then(function(dare){
    return dare.toogleFavorite(req.user);
  }).then(function(){
    res.json({status: "success"});
  })
  .catch(function(err) {
    res.json(err);
  });
}

function send(req, res){
  const { id, duration, title, description, points } = req.body;
  if(!req.user.couple){
    res.json({success: 'fail' })
    return false
  }
  if (id && duration) {
    var promise = Dare.findOne({_id: id}).exec();
  } else if (title && description && points && duration){
    var newDare = new Dare({
        title: title,
        description: description,
        points: points,
        type: 'writted',
      });
    var promise = newDare.save();
  } else {
    var promise = Promise.reject({message:"Invalid params"});
  }
  var sendDare;
  promise.then((dare)=>{
    return dare.send(req.user, duration).then((couple)=>{
      return [couple, dare]
    });
  })
  .then(([couple, dare])=>{
    res.json({status: "success", data:{
      dare: dare,
      sendedDare: _.last(couple.dares),
    }});
  })
  .catch((err)=>{
    res.json(err)
  });
}


module.exports = {
  accept: acctionPattern('accept'),
  reject: acctionPattern('reject'),
  complete: acctionPattern('complete'),
  fail: acctionPattern('fail'),
  getAll: getAll,
  getOne: getOne,
  update: update,
  toogleFavorite: toogleFavorite,
  send: send,
}
