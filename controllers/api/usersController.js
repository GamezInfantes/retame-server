'use strict'

var User = require('server/models/user');
var _ = require('underscore');

function getOne(req, res) {
  var query = User.findOne({
    _id: req.params.id,
  });
  query.select(
    `email username followers points gender image role follow`
  );
  if (req.user._id == req.params.id) {
    query.select(
      `displayName couple settings dares completedBefore favoriteDares points`
    )
  }
  query.exec().then((user)=>{
    res.json({
      status: 'success',
      data: user,
    })
  })
}

function update(req, res) {
  if (req.user._id == req.params.id) {
    Object.assign(
      req.user,
      _.pick(req.body, 'displayName', 'settings', 'gender', 'image', 'devices')
    );
    req.user.save().then((user)=>{
      res.json({
        status: 'success',
        data: user
      })
    }).catch((err)=>{
      res.json({
        status: 'fail',
        message: err,
      })
    })
  } else {
    res.status(403).json({
      status: 'fail',
    })
  }
}

function create(req, res) {
  const { email, password, username } = req.body;

  let user = new User({ email, password, username });
  user.save().then(()=>{
    res.json({
      status: 'success',
      data: {}
    })
  })

}

module.exports = { getOne, update, create }
