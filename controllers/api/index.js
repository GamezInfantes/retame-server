'use strict';
var _ = require('underscore');

var Category = require('server/models/category');
var Dare = require('server/models/dare');
var User = require('server/models/user');

var daresController = require('./daresController');
var usersController = require('./usersController');
var categoriesController = require('./categoriesController');
var authenticateController = require('./authenticateController');


function init(req, res) {
  var response = { data: {}, status: 'success' }

  var _categories;
  Category.targered(req.user).exec()
  .then(function(categories){
    _categories = categories
    var daresCategories = []
    categories.map((category)=> {
      category.targered(req.user);
      daresCategories = daresCategories.concat(category.dares);
    });
    var daresUser = req.user.dares.map(function(dare){
      return dare.dare;
    });

    var daresCouple = []
    if (req.user.couple) {
      daresCouple = req.user.couple.dares.map(function(dare){
        return dare.dare;
      });
    }

    return _.uniq(daresCategories.concat(daresUser).concat(daresCouple))
  })
  .then(function(daresIds){
    return Dare.find({_id: {$in: daresIds} }).exec()
  })
  .then(function(dares){
    response.data = {
      categories: _categories,
      dares: dares,
      user: req.user.public(),
    }
    res.json(response);
  })
  .catch(function(err) {
    console.log(err);
    res.send({
      status: 'fail'
    });
  });
}

module.exports = {
  dares: daresController,
  authenticate: authenticateController,
  init: init,
  users: usersController,
  categories: categoriesController,
};
