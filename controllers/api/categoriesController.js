'use strict'
var mongoose = require('mongoose');
var Category = require('server/models/category');
var Dare = require('server/models/dare');
var Promise = require('bluebird');
var _ = require('underscore');


function getOne(req,res) {
  Category.findOne({ published: true, _id: req.params.id})
  .then((category)=>{
    category.targered(req.user);
    return _.pick(category,
      'id', 'name', 'image', 'looked', 'dares', 'dares_count'
    );
  })
  .then((category)=>{
    console.log(category.dares);
    return Dare.find({
      published:true, _id: {$in: category.dares }
    })
    .select(
      `title description points image language
      timesFavorite timesSended timesCompleted published`
    )
    .then((dares)=>{
      category.dares = dares;
      return category
    })
  })
  .then((category)=>{
    if (req.user.categoriesUnlooked.indexOf(req.params.id) == -1 && category.looked) {
      res.status(403).json({
        status: 'fail',
        messsaje: 'category looked'
      });
      return;
    }
    res.json({
      status: 'success',
      data: category,
    })
  })
  .catch(function (err){
    console.log(err);
    res.json({
      status: 'fail',
      // message: err
    })
  })
}


module.exports = {
  getOne
}
