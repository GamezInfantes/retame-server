'user strict';

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');
var api = require('./routes/api');

var middlewares = require('./middlewares');
var loginRequired = require('./middlewares').loginRequired;
var authenticate = require('./middlewares').authenticate;

var mongoose = require( 'mongoose' );

var bluebird = require('bluebird');
global.Promise = bluebird;


var apiControllers = require('./controllers/api');


mongoose.Promise = bluebird
// Create the database connection
mongoose.connect('mongodb://localhost/retame');

mongoose.connection.on('error', function (err) {
  console.log('Mongoose default connection error: ' + err);
});

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use('/static', require('stylus').middleware(path.join(__dirname, 'public')));
app.use('/static', express.static(path.join(__dirname, 'public')));


/**
 * Allow cross Origin
 */
app.use('/api', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  next();
}).options('*', function(req, res, next){
  res.status(200).end();
});


/**
 *	Adds user to request
 */
app.use(authenticate);

app.post('/api/authenticate/', require('./controllers/api').authenticate.index);
app.post('/api/authenticate/refresh/', require('./controllers/api').authenticate.refresh);
app.post('/api/users/', apiControllers.users.create)

app.use('/api', loginRequired, api);

app.use('/', routes);

/**
 *
 */
var apiRest = require('server/routes/apirest');
apiRest(app);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
