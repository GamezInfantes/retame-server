var mongoose = require( 'mongoose' );

// Create the database connection
mongoose.connect('mongodb://localhost/retame');

var Category = require('./models/category');
var Dare = require('./models/dare');


var dares = [
   {
      "id":1,
      "title":"Ñammy!!",
      "description":"Que te parece si hacemos un libro de recetas que sea solo nuestro? Tu reto consite en crear una receta nueva y cocinarla para mí!. La primera receta la añadiremos a a nuestro libro... Sé creativo!",
      "points":25,
      "views":145,
      "author":{
         "username":"rubita"
      },
      "published": true
   },
   {
      "id":2,
      "title":"Entrevista de trabajo",
      "description":"Usted ha sido selecionado para participar en la ultima etapa del proceso de seleccion para el cargo de Asisten Personal de nuestro Gerente General, el cual relizará personalmente la Entrevista, la que contara de pruebas tanto orales y físicas. Deberá asistir con vestimenta formal",
      "points":35,
      "views":263,
      "author":{
         "username":"rubita"
      },
      "published": true
   },
   {
      "id":3,
      "title":"test",
      "description":"Uste tanto orales y físicas. Deberá asistir con vestimenta formal",
      "points":35,
      "views":263,
      "author":{
         "username":"Liia"
      },
      "published": true
   }
];

// Dare.create(dares, function(err) {
//   console.log(err);
// });

var user = {
  "email": "gamez@correo.com",
  "username": "gamez",
  "password": "1234",
  "displayName": "Cristian",
  "gender": "male",
  "favoritesDares": [],
  "completedBefore": [],
  "dares": [],
  "follow": [],
  "devices":[]
}


var categories = [
   Category({
      "id":1,
      "img":"https://s-media-cache-ak0.pinimg.com/736x/aa/b9/fa/aab9faed17e80e48252d5da4c1268b56.jpg",
      "name":"En el baño",
      "dareCount":"11",
      "dares":[1,2,3],
      "published": true
   }),
   Category({
      "id":2,
      "img":"http://cache4.asset-cache.net/gc/553202161-couple-in-kitchen-sharing-an-int…d=fL13l%2BeRu4SY7yu7SmOfkXxl4JjB%2BD%2FUkmFYf%2FrW2nw99BgWQsUSpdwYkXyPSoRf",
      "name":"Cocina",
      "dareCount":"11",
      "dares":[],
      "published": true
   }),
   Category({
      "id":3,
      "img":"http://totalnewswire.com/files/2014/03/Policeman.jpg",
      "name":"A mis ordenes",
      "dareCount":"11",
      "dares":[],
      "published": true
   }),
   Category({
      "id":4,
      "img":"https://40.media.tumblr.com/tumblr_m954umzout1rch21yo1_1280.jpg",
      "name":"Dulce amor",
      "dareCount":"11",
      "dares":[],
      "published": true
   })
];
for (var i = 0; i < categories.length; i++) {
  // categories[i].save();
}
