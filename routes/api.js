'use strict';

var express = require('express');
var router = express.Router();

var apiControllers = require('../controllers/api');
var generateREST = require('../utils').generateREST;


router.get('/', function(req, res, next) {
  res.json({})
});


router.get('/init/', apiControllers.init);
//dare routes
router.post('/dares/send/', apiControllers.dares.send);
router.post('/dares/accept/', apiControllers.dares.accept);
router.post('/dares/reject/', apiControllers.dares.reject);
router.post('/dares/complete/', apiControllers.dares.complete);
router.post('/dares/fail/', apiControllers.dares.fail);
router.post('/dares/:id/toogleFavorite/', apiControllers.dares.toogleFavorite)
router.get('/dares/', apiControllers.dares.getAll)
router.get('/dares/:id/', apiControllers.dares.getOne)
router.put('/dares/:id/', apiControllers.dares.update)

// User routes
router.get('/users/:id/', apiControllers.users.getOne)
router.put('/users/:id/', apiControllers.users.update)
router.post('/users/', apiControllers.users.create)

// route refresh token
router.post('/authenticate/refresh/', apiControllers.authenticate.refresh);


router.get('/categories/:id/', apiControllers.categories.getOne);



module.exports = router;
