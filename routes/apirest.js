var restful = require('node-restful');
var categorySchema = require('server/models/category').schema;
var userSchema = require('server/models/user').schema;
var dareSchema = require('server/models/dare').schema;

module.exports = function (app) {
  var DareResouce = restful.model('dare', dareSchema)
    .methods(['get', 'post', 'put', 'delete']);

  var CategoryResource = restful.model('category', categorySchema)
    .methods(['get', 'post', 'put', 'delete']);

  var UserResource = restful.model('user', userSchema)
    .methods(['get', 'post', 'put', 'delete']);


  DareResouce.register(app, '/api/v2/dares');
  UserResource.register(app, '/api/v2/users');
  CategoryResource.register(app, '/api/v2/categories');
  
}
