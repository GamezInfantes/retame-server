var experssJwt = require('express-jwt');
var User = require('server/models/user');
var config = require('server/config');


exports.loginRequired = function (req, res, next) {
  if (typeof req.user !== "undefined"){
      return next();
    } else{
     res.status(401).json({success: 'fail', message: 'Required authentication' });
  }
};

function hidrateUser(req, res, next){
  if(typeof req.token == "undefined"){
    next(); return;
  }
  User.findOne({_id: req.token.userId}).populate('couple').exec().then((user)=>{
    req.user = user;
    next();
  });
};

exports.authenticate = [
   experssJwt({
      secret: config.JWT_SECRET,
      credentialsRequired: false,
      requestProperty: 'token'
    }),
    hidrateUser
 ];
